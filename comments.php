<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php function threadedComments($comments, $options) {
    $commentClass = '';
    if ($comments->authorId) {
        if ($comments->authorId == $comments->ownerId) {
            $commentClass .= ' comment-by-author';
        } else {
            $commentClass .= ' comment-by-user';
        }
    }
    $commentLevelClass = $comments->levels > 0 ? ' comment-child' : ' comment-parent';
?>
<li id="<?php $comments->theId(); ?>" class="comment-body<?php 
if ($comments->levels > 0) {
    echo ' comment-child';
    $comments->levelsAlt(' comment-level-odd', ' comment-level-even');
} else {
    echo ' comment-parent';
}
$comments->alt(' comment-odd', ' comment-even');
echo $commentClass;
?>">
<div id="<?php $comments->theId(); ?>">
<?php if ($comments->levels > 0): ?>
<div class="comment-content">
<strong><?php $comments->author(); ?></strong>：<?php echo get_comment_at($comments->coid); ?> <a href="#comment-<?php $comments->coid(); ?>" rel="nofollow" onclick="return TypechoComment.reply('comment-<?php $comments->coid(); ?>', <?php $comments->coid(); ?>);"><?php $comments->content(); ?></a>
</div>
</div>
<?php else: ?>
<div class="comment-author">
<img loading="lazy" class="avatar" src="<?php echo getAvatarByMail($comments->mail, $comments->authorId); ?>">
<cite class="fn"><?php $comments->author(); ?></cite>
</div>
<time><?php $comments->date('Y-m-d H:i'); ?></time>
<div class="comment-content">
<?php $comments->content(); ?>
</div>
<div class="comment-reply"><?php $comments->reply('回复'); ?></div>
</div>
<?php if ($comments->children) { ?>
<hr>
<div class="comment-children">
<?php $comments->threadedComments($options); ?>
</div>
<?php } ?>
<?php endif; ?>
</li>
<?php } ?>
<div id="comments">
<?php $this->comments()->to($comments); ?>
<?php if ($comments->have()): ?>
<?php $comments->listComments(); ?>
<?php $comments->pageNav('<', '>', 1, '...', array('wrapTag' => 'nav', 'wrapClass' => 'reade_more','itemTag' => '','currentClass' => 'current')); ?>
<?php endif; ?>
<?php if($this->allow('comment')): ?>
<div id="<?php $this->respondId(); ?>" class="respond">
<h3 id="response">发表<small><?php $comments->cancelReply('取消回复'); ?></small></h3>
<form method="post" action="<?php $this->commentUrl() ?>" id="commentform" role="form">
<?php if($this->user->hasLogin()): ?>
<p><?php _e('登录身份: '); ?><a href="<?php $this->options->profileUrl(); ?>"><?php $this->user->screenName(); ?></a>. <a href="<?php $this->options->logoutUrl(); ?>" title="Logout"><?php _e('退出'); ?> &raquo;</a></p>
<?php else: ?>
<p>
<input type="text" name="author" id="author" class="text" placeholder="<?php _e('昵称 *'); ?>" value="<?php $this->remember('author'); ?>" required />
</p>
<p>
<input type="email" name="mail" id="mail" class="text" placeholder="<?php _e('邮箱 *'); ?>" value="<?php $this->remember('mail'); ?>"<?php if ($this->options->commentsRequireMail): ?> required<?php endif; ?> />
</p>
<p>
<input type="url" name="url" id="url" class="text" placeholder="<?php _e('网址'); ?>" value="<?php $this->remember('url'); ?>"<?php if ($this->options->commentsRequireURL): ?> required<?php endif; ?> />
</p>
<?php endif; ?>
<p>
<textarea rows="8" cols="50" name="text" id="textarea" class="textarea" placeholder="<?php _e('请输入内容...'); ?>" required><?php $this->remember('text'); ?></textarea>
</p>
<p>
<button type="submit" id="submit" class="submit">提交</button>
</p>
</form>
</div>
<?php endif; ?>
</div>
<script type="text/javascript">
(function () {
    window.TypechoComment = {
        dom : function (id) {
            return document.getElementById(id);
        },
    
        create : function (tag, attr) {
            var el = document.createElement(tag);
        
            for (var key in attr) {
                el.setAttribute(key, attr[key]);
            }
        
            return el;
        },

        reply : function (cid, coid) {
            var comment = this.dom(cid), parent = comment.parentNode,
                response = this.dom('<?php $this->respondId(); ?>'), input = this.dom('comment-parent'),
                form = 'form' == response.tagName ? response : response.getElementsByTagName('form')[0],
                textarea = response.getElementsByTagName('textarea')[0];

            if (null == input) {
                input = this.create('input', {
                    'type' : 'hidden',
                    'name' : 'parent',
                    'id'   : 'comment-parent'
                });

                form.appendChild(input);
            }

            input.setAttribute('value', coid);

            if (null == this.dom('comment-form-place-holder')) {
                var holder = this.create('div', {
                    'id' : 'comment-form-place-holder'
                });

                response.parentNode.insertBefore(holder, response);
            }

            comment.appendChild(response);
            this.dom('cancel-comment-reply-link').style.display = '';

            if (null != textarea && 'text' == textarea.name) {
                textarea.focus();
            }

            return false;
        },

        cancelReply : function () {
            var response = this.dom('<?php $this->respondId(); ?>'),
            holder = this.dom('comment-form-place-holder'), input = this.dom('comment-parent');

            if (null != input) {
                input.parentNode.removeChild(input);
            }

            if (null == holder) {
                return true;
            }

            this.dom('cancel-comment-reply-link').style.display = 'none';
            holder.parentNode.insertBefore(response, holder);
            return false;
        }
    };
})();
</script>