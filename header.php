<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<link rel="icon" href="<?php echo getOptionValueOrDefault('icon', $this->options->siteUrl . 'favicon.ico'); ?>">
<link rel="stylesheet" href="<?php echo getOptionValueOrDefault('cdnAddress', $this->options->themeUrl . '/'); ?>style.css">
<?php if ($this->is('single')): ?>
<meta property="og:type" content="article"/>
<meta property="og:url" content="<?php $this->permalink(); ?>"/>
<meta property="og:title" content="<?php $this->archiveTitle('','',' - '); ?><?php if($this->_currentPage>1) echo '第'.$this->_currentPage.'页 - '; ?><?php $this->options->title(); ?>"/>
<meta property="og:author" content="<?php $this->author->screenName(); ?>"/>
<meta property="og:type" content="article"/>
<meta property="article:published_time" content="<?php $this->date('c'); ?>"/>
<meta property="article:modified_time" content="<?php echo date('c', $this->modified); ?>"/>
<meta property="article:published_first" content="<?php $this->options->title(); ?>, <?php $this->permalink(); ?>"/>
<?php else: ?>
<meta property="og:type" content="website"/>
<meta property="og:url" content="<?php $this->options->siteUrl(); ?>"/>
<meta property="og:title" content="<?php $this->archiveTitle(['category' => _t('%s'),'search' => _t('包含%s的内容'),'tag' => _t('标签%s的内容'),'author' => _t('%s发布的内容'),],'',' - '); ?><?php if($this->_currentPage>1) echo '第'.$this->_currentPage.'页 - '; ?><?php $this->options->title(); if ($this->is('index')): ?><?php echo ' - 在平凡的世界里保持幻想'; endif; ?>"/>
<meta property="og:author" content="<?php $this->options->title(); ?>"/>
<?php endif; ?>
<title><?php $this->archiveTitle(['category' => _t('%s'),'search' => _t('包含%s的内容'),'tag' => _t('标签%s的内容'),'author' => _t('%s发布的内容'),],'',' - '); ?><?php if($this->_currentPage>1) echo '第'.$this->_currentPage.'页 - '; ?><?php $this->options->title(); if ($this->is('index')): ?><?php echo ' - 在平凡的世界里保持幻想'; endif; ?></title>
<?php $this->header('generator=&template=&pingback=&xmlrpc=&wlw=&commentReply='); ?>
</head>
<body>
<section class="container">
<header class="header">
<div class="webname"><?php if (!$this->is('single')): ?><h1><a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title(); ?></a></h1><?php else: ?><a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title(); ?></a><?php endif; ?></div>
<div class="menu">
<?php $menus = gettopMenu(); ?><?php if (!empty($menus)): ?><?php foreach ($menus as $counter => $menu): ?><?php if ($counter !== 0) {echo ' · '; }; ?><a href="<?php echo $menu['url']; ?>"><?php echo $menu['name']; ?></a><?php endforeach; ?><?php endif; ?>
</div>
</header>
</section>
<div class="container">