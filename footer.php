<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
</div>
<section class="container">
<footer class="footer">
<div class="menu">
<?php $menus = getbottomMenu(); ?><?php if (!empty($menus)): ?><?php foreach ($menus as $counter => $menu): ?><?php if ($counter !== 0) {echo ' · '; }; ?><a href="<?php echo $menu['url']; ?>"><?php echo $menu['name']; ?></a><?php endforeach; ?><?php endif; ?>
</div>
<p>&copy; <?php echo getCopyrightDate(); ?> <a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title(); ?></a> · All Rights Reserved<?php if ($this->options->icpCode): ?> · <a href="https://beian.miit.gov.cn/" target="_blank" rel="external nofollow"><?php $this->options->icpCode(); ?></a><?php endif; ?><?php if ($this->options->gaCode): ?> · <a href="https://beian.mps.gov.cn/" target="_blank" rel="external nofollow"><?php $this->options->gaCode(); ?></a><?php endif; ?></p>
</footer>
</section>
</body>
<?php $this->footer(); ?>
</html>