<?php
/**
 * 检索
 *
 * @package custom
 */
if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<article class="post_article" itemscope itemtype="https://schema.org/Article">
<h1 itemprop="name headline"><?php $this->title(); ?></h1>
<form method="post" action="" class="search" itemprop="potentialAction" itemscope itemtype="https://schema.org/SearchAction">
<input type="text" name="s" class="search_key" placeholder="输入关键词..." itemprop="query-input">
</form>
</article>
<?php $this->widget('Widget_Metas_Tag_Cloud', 'ignoreZeroCount=1&desc=1')->to($tags); ?>
<?php if($tags->have()): ?>
<ul class="tags">
<?php while ($tags->next()): ?>
<li><a href="<?php $tags->permalink(); ?>"><?php $tags->name(); ?></a></li>
<?php endwhile; ?>
</ul>
<?php endif; ?>
<?php $this->need('footer.php'); ?>