<?php
/**
 * 文章归档
 *
 * @package custom
 */
if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<article class="post_article" itemscope itemtype="https://schema.org/Article">
<h1 itemprop="name headline"><?php $this->title(); ?></h1>
<?php
 $archives = getArchives($this);
 $number = 0;
 foreach ($archives as $year => $posts) {
   $open = $number === 0 ? null : 'closed';
   echo '<h2>' .$year .'年</h2>';
   echo '<ul id="archive-list-' .$year .'" class="archive-list' .$open .'">';
   foreach ($posts as $created => $post) {
     echo '<li class="archive-item"><span class="archive-date">' .date("m-d", $created) .'</span> · <a href="' .$post["permalink"] .'">' .$post["title"] .'</a></li>';
   }
   echo '</ul>';
   $number++;
 }
 ?>
 </article>
<?php $this->need('footer.php'); ?>