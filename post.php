<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<article class="post_article" itemscope itemtype="https://schema.org/Article">
<h1 itemprop="name headline"><?php $this->title(); ?></h1>
<?php if ($this->options->PostAD): ?>
<?php $this->options->PostAD(); ?>
<?php endif; ?>
<main itemprop="articleBody">
<?php $this->content(); ?>
</main>
<time datetime="<?php $this->date('c'); ?>" itemprop="datePublished">发布于：<?php $this->date('Y-m-d H:i');?></time>
<p>> 快来<strong><a href="https://buqi.cn/say/">说说</a></strong>你的想法吧~</p>
</article>
<ul class="tags"><li><?php $this->category('</li><li>', true, 'none'); ?></li><li><a href="<?php $this->author->permalink(); ?>"><?php $this->author->screenName(); ?></a></li><?php if(count($this->tags) > 0 ): ?><li><?php $this->tags('</li><li>', true, 'none'); ?></li><?php endif; ?></ul>
<?php $this->need('footer.php'); ?>