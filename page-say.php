<?php
/**
 * 说说
 *
 * @package custom
 */
if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<article class="post_article" itemscope itemtype="https://schema.org/Article">
<h1 itemprop="name headline"><?php $this->title(); ?></h1>
<?php
// 判断JSON数据是否存在且可读
if (is_writeable('./xxvvrssoutput.json')) {
// 获取JSON数据
$jsonData = file_get_contents('./xxvvrssoutput.json');
// 将JSON数据解析为PHP数组
$articles = json_decode($jsonData, true);
// 对文章按时间排序（最新的排在前面）
usort($articles, function ($a, $b) {
return strtotime($b['time']) - strtotime($a['time']);
});
// 设置每页显示的文章数量
$itemsPerPage = 5;
// 生成文章列表
foreach (array_slice($articles, 0, $itemsPerPage) as $article) {; ?>
<p><strong><?php echo htmlspecialchars($article['site_name']); ?></strong>：<a href="<?php echo htmlspecialchars($article['link']); ?>" target="_blank" rel="external nofollow"><?php echo htmlspecialchars($article['title']); ?></a>
<?php }}; ?>
</article>
<?php $this->need('comments.php'); ?>
<?php $this->need('footer.php'); ?>