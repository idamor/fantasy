<?php 
/**
 * 抱不器之器，成有用之用
 *
 * @package Booky
 * @author Booky
 * @version 1.0
 * @link https://buqi.cn/
 */
 if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<div class="post-list">
<?php if ($this->have()): ?>
<?php if ($this->is('index') && $this->_currentPage == 1): ?>
<?php $cids = getStickyPost(); if (null != $cids): ?>
<?php foreach ($cids as $cid): ?>
<?php $this->widget('Widget_Archive@Fantasy' . $cid, 'pageSize=1&type=post', 'cid=' . $cid)->to($item); ?>
<article class="meta" itemscope itemtype="https://schema.org/Article">
<header>
<h2 itemprop="name headline"><a href="<?php $item->permalink(); ?>" itemprop="url"><?php $item->title(); ?></a> <span class="mark">[顶]</span></h2>
</header>
<main itemprop="articleBody">
<p><?php $item->excerpt(100, '...'); ?></p>
</main>
<footer>
<span datetime="<?php $this->date('c'); ?>" itemprop="datePublished"><?php $item->date('Y-m-d'); ?></span> · <span itemprop="author" itemscope itemtype="https://schema.org/Person"><?php $item->author->screenName(); ?></span>
</footer>
</article>
<?php endforeach; ?>
<?php endif; ?>
<?php endif; ?>
<?php while ($this->next()): ?>
<article class="meta" itemscope itemtype="https://schema.org/Article">
<header>
<h2 itemprop="name headline"><a href="<?php $this->permalink(); ?>" itemprop="url"><?php $this->title(); ?></a></h2>
</header>
<main itemprop="articleBody">
<p><?php $this->excerpt(100, '...'); ?></p>
</main>
<footer>
<span datetime="<?php $this->date('c'); ?>" itemprop="datePublished"><?php $this->date('Y-m-d'); ?></span> · <span itemprop="author" itemscope itemtype="https://schema.org/Person"><?php $this->author->screenName(); ?></span>
</footer>
</article>
<?php endwhile; ?>
<?php $this->pageNav('<', '>', 1, '...', array('wrapTag' => 'nav', 'wrapClass' => 'reade_more','itemTag' => '','currentClass' => 'current')); ?>
<?php else: ?>
<h3>Sorry!</h3>
<p>没有你要找的内容哦~</p>
<?php endif; ?>
</div>
<?php $this->need('footer.php'); ?>